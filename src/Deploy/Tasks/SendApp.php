<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;
use CWE\Provender\CLI;
use CWE\Libraries\ObjectRex;
use CWE\Provender\Plugins\Deploy\Helpers\Filesystem;

class SendApp implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->eventEmitter = &$resources['eventEmitter'];
        $this->config = $resources['config']['deploy'];
        $connections = $results[count($results)-1];
        $remoteLocation = $results[0]['remoteLocation'];
        $zipLocation = $results[count($results)-3];

        foreach ($connections as $host => $conn) {
            $stream = ssh2_exec(
                $conn,
                "echo $remoteLocation"
            );
            stream_set_blocking($stream, true);
            $remoteLocation = trim(fread($stream, 4096));
            fclose($stream);

            $this->logger->logInfo(
                "\t[local] Sending App"
            );
            $result = ssh2_scp_send(
                $conn,
                $zipLocation,
                $remoteLocation.'/.provender/releases/'.basename($zipLocation)
            );

            if ($result) {
                $this->logger->logInfo(
                    "\t[local] Sent App to $host"
                );
            } else {
                $this->logger->logError(
                    "\t[local] Failed to send App to $host"
                );
                continue;
            }

            $this->logger->logInfo("\t[local] Unziping release");
            // TODO: Unzip to a directory
            $stream = ssh2_exec(
                $conn,
                'unzip ' . $remoteLocation . '/.provender/releases/'
                . basename($zipLocation) . ' -d ' . $remoteLocation . '/'
                . basename($zipLocation, '.zip')
            );
            $stderr = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
            stream_set_blocking($stream, true);
            stream_set_blocking($stderr, true);
            $cmd = fread($stream, 4096);
            $errs = fread($stderr, 4096);
            fclose($stream);

            // Copy directory/.provender to .provender
            $shell = ssh2_shell($conn);
            // $stderr = ssh2_fetch_stream($shell, SSH2_STREAM_STDERR);
            stream_set_blocking($shell, false);
            // stream_set_blocking($stderr, true);
            
            fwrite(
                $shell,
                "cd $remoteLocation\n"
            );
            sleep(1);
            while ($cmd = fgets($shell, 4096)) {
                continue;
            }

            $releaseFolder = basename($zipLocation, '.zip');

            $this->logger->logInfo("\t[local] Updating remote Provender Project");
            fwrite(
                $shell,
                "cp -R $releaseFolder/.provender ./\n"
            );
            sleep(1);
            while ($cmd = fgets($shell, 4096)) {
                continue;
            }

            $this->logger->logInfo("\t[local] Running deploy:aquire");
            fwrite(
                $shell,
                "~/.composer/vendor/bin/provender deploy:aquire {$options[2]} --release=$releaseFolder\n"
            );
            sleep(1);
            $output = '';
            while ($cmd = fgets($shell, 4096)) {
                $output .= $cmd;
            }
            
            fclose($shell);
            $find = 'deploy:aquire:complete';
            $complete = strpos($output, $find);
            $completeLen = strlen($find);
            $firstTwo = strpos(
                $output,
                "\n",
                strpos($output, "\n")
            )+1;
            if ($complete === false) {
                $this->logger->logError(
                    "\t[local] deploy:aquired failed on $host"
                );
                continue;
            } else {
                $this->logger->log(substr(
                    $output,
                    $firstTwo,
                    $complete-$firstTwo+$completeLen
                ));
                $this->logger->logInfo(
                    "\t[local] deploy:aquired completed on $host"
                );
                // yield $host => $conn;
            }

        }
    }
}
