<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;

class CheckAppSet implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->eventEmitter = &$resources['eventEmitter'];
        $this->config = $resources['config']['deploy'];

        if (!isset($options[2])) {
            $this->logger->logError(
                "[local] No app_set specified"
            );
            $this->logger->log();
            $this->eventEmitter->emit(new Event('help:deploy:start', []));
            return false;
        } else {
            $app_set = $options[2];
            $this->logger->logInfo(
                "[local] app_set is $app_set"
            );
        }
        
        if (!isset($this->config['app_sets'])) {
            $this->logger->logError(
                "[local] No app_sets in config"
            );
            $this->logger->log();
            return false;
        }

        
        if (!isset($this->config['app_sets'][$app_set])) {
            $this->logger->logError(
                "[local] Missing app_set '$app_set' in config"
            );
            $this->logger->log();
            return false;
        } else {
            $this->logger->logInfo(
                "[local] app_set $app_set found in config"
            );
        }

        if (!isset($this->config['app_sets'][$app_set]['servers'])) {
            $this->logger->logError(
                "[local] Missing app_set server list in config"
            );
            $this->logger->log();
            return false;
        } else {
            $this->logger->logInfo(
                "[local] app_set server list found in config"
            );
        }

        if (array_key_exists('class', $this->config['app_sets'][$app_set])) {
            $class = $this->config['app_sets'][$app_set]['class'];
            if (!class_exists($class)) {
                $this->logger->logError(
                    "[local] Dynamic app_set class $class not found"
                );
                $this->logger->log();
                return false;
            }
            
        }

        return $this->config['app_sets'][$app_set];
    }
}
