<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;
use CWE\Provender\Plugins\Deploy\Helpers\Filesystem;

class CheckKeys implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->eventEmitter = &$resources['eventEmitter'];
        $this->config = $resources['config']['deploy'];
        
        if (!isset($this->config['ssh_key'])) {
            $this->logger->logError(
                "[local] No ssh key name in config"
            );
            $this->logger->log();
            return false;
        } else {
            $keyName = $this->config['ssh_key'];
        }

        if (!isset($this->config['ssh_key_location'])) {
            $this->logger->logError(
                "[local] No ssh key location in config"
            );
            $this->logger->log();
            return false;
        } else {
            $keyLocation = Filesystem::expandPath(
                $this->config['ssh_key_location']
            );
        }

        if (!file_exists("$keyLocation/$keyName")) {
            $this->logger->logError(
                "[local] No ssh private key not found"
            );
            $this->logger->log();
            return false;
        } else {
            $this->logger->logInfo(
                "[local] ssh private key found"
            );
        }

        if (!file_exists("$keyLocation/$keyName.pub")) {
            $this->logger->logError(
                "[local] No ssh public key not found"
            );
            $this->logger->log();
            return false;
        } else {
            $this->logger->logInfo(
                "[local] ssh public key found"
            );
        }


        return true;
    }
}
