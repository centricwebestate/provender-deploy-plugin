<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;

class CheckStrategy implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->config = $resources['config']['deploy'];
        
        if (!isset($this->config['strategy'])) {
            $this->logger->logError(
                "[local] No deployment strategy specified"
            );
            $this->logger->log();
            return false;
        }

        if (!class_exists($this->config['strategy'])) {
            $this->logger->logError(
                "[local] {$this->config['strategy']} not found"
            );
            $this->logger->log();
            return false;
        } else {
            $this->logger->logInfo(
                "[local] {$this->config['strategy']} found"
            );
        }

        return true;
    }
}
