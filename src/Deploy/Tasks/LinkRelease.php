<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;

class LinkRelease implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->eventEmitter = &$resources['eventEmitter'];
        $this->config = $resources['config']['deploy'];

        $this->logger->logInfo("\t[server] Linking current to the latest release");
        $release = $options['release'];
        shell_exec("ln -sfn ./$release ./current");
        return true;
    }
}
