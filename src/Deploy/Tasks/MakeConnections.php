<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;
use CWE\Provender\CLI;
use CWE\Libraries\ObjectRex;
use CWE\Provender\Plugins\Deploy\Helpers\Filesystem;

class MakeConnections implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->eventEmitter = &$resources['eventEmitter'];
        $this->config = $resources['config']['deploy'];
        $this->servers = $results[0]['servers'];
        $connections = [];

        $keyLocation = $this->config['ssh_key_location'];
        $key = $this->config['ssh_key'];

        $publicKey = Filesystem::expandPath("$keyLocation/$key.pub");
        $privateKey = Filesystem::expandPath("$keyLocation/$key");

        if ($this->config['ssh_passkey']) {
            print "PrivateKey Passkey: ";
            $passkey = fgets(STDIN);
        } else {
            $passkey = null;
        }

        foreach ($this->servers as $dsn) {
            $dsn = parse_url($dsn);
            if (!isset($dsn['port'])) {
                $dsn['port'] = 22;
            }

            $conn = ssh2_connect(
                $dsn['host'],
                $dsn['port'],
                array('hostkey'=>'ssh-rsa')
            );

            if (!$conn) {
                $this->logger->logError(
                    "\t[local] Failed to connect to server: {$dsn['host']}"
                );
                continue;
            }

            $auth = ssh2_auth_pubkey_file(
                $conn,
                $dsn['user'],
                $publicKey,
                $privateKey,
                $passkey
            );

            if (!$auth) {
                $this->logger->logError(
                    "\t[local] Failed to auth to server: {$dsn['host']}"
                );
                continue;
            } else {
                $this->logger->logInfo(
                    "\t[local] Connection to {$dsn['host']} successful"
                );
                yield $dsn['host'] => $conn;
            }

            
        }

    }
}
