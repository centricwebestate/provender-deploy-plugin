<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;
use CWE\Provender\Plugins\Deploy\Helpers\Filesystem;
use CWE\Provender\CLI;

class CleanReleases implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->eventEmitter = &$resources['eventEmitter'];
        $this->config = $resources['config']['deploy'];
        
        $provenderDir = CLI::findProjectFolder(__DIR__);
        if (isset($options['clean'])) {
            $this->logger->logInfo(
                "[local] Cleaning release folder"
            );

            $releaseDir = "$provenderDir/releases";
            if (file_exists($releaseDir)) {
                foreach (glob("$releaseDir/*") as $file) {
                    unlink($file);
                }
            } else {
                $this->logger->logWarn(
                    "[local] No release folder found. Continuing"
                );
                return true;
            }

            $this->logger->logInfo(
                "[local] Release folder cleaned"
            );
        }

        return true;
    }
}
