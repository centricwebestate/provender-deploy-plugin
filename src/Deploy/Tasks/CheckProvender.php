<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;
use CWE\Provender\CLI;
use CWE\Libraries\ObjectRex;
use CWE\Provender\Plugins\Deploy\Helpers\Filesystem;

class CheckProvender implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->eventEmitter = &$resources['eventEmitter'];
        $this->config = $resources['config']['deploy'];
        $connections = $results[count($results)-1];
        $remoteLocation = $results[0]['remoteLocation'];

        foreach ($connections as $host => $conn) {
        // var_dump($results);exit;
            $stream = ssh2_exec(
                $conn,
                "composer --version"
            );
            stream_set_blocking($stream, true);
            $cmd = fread($stream, 4096);
            fclose($stream);

            if (empty($cmd)) {
                $this->logger->logWarn(
                    "\t[local] Composer not installed. Skipping server $host"
                );
                continue;
            } else {
                $this->logger->logInfo("\t[local] Composer installed");
            }

            // Always mkdir the provender folder JIC location was changed
            $stream = ssh2_exec(
                $conn,
                "mkdir -p $remoteLocation/.provender/releases"
            );
            stream_set_blocking($stream, true);
            $cmd = fread($stream, 4096);
            fclose($stream);

            if (!empty($cmd)) {
                $this->logger->logWarn(
                    "\t[local] Unexpected message when creating dir"
                );
                $this->logger->log("\t[local] $cmd");
                continue;
            } else {
                $this->logger->logInfo("\t[local] Provender dir exists");
            }

            $stream = ssh2_exec(
                $conn,
                "~/.composer/vendor/bin/provender version"
            );
            $serr = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
            stream_set_blocking($stream, true);
            stream_set_blocking($serr, true);
            $cmd = fread($stream, 4096);
            $cmderr = fread($serr, 4096);
            fclose($stream);

            if (empty($cmd)) {
                $this->logger->logWarn(
                    "\t[local] Provender not installed"
                );
                $this->logger->log($cmd);
                $this->logger->log($cmderr);
            } else {
                $this->logger->logInfo("\t[local] Provender installed");
                yield $host => $conn;
                continue;
            }

            $stream = ssh2_exec(
                $conn,
                "composer global require centricwebestate/provender"
            );
            $serr = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
            stream_set_blocking($stream, true);
            stream_set_blocking($serr, true);
            $cmd = fread($stream, 4096);
            $cmderr = fread($serr, 4096);
            fclose($stream);

            if (!empty(trim($cmderr))) {
                $this->logger->logWarn(
                    "\t[local] Failed Installing Provender"
                );
                continue;
            } else {
                $this->logger->logInfo(
                    "\t[local] Composer installed Provender"
                );
            }

            $stream = ssh2_exec(
                $conn,
                "~/.composer/vendor/bin/provender-install"
            );
            $serr = ssh2_fetch_stream($stream, SSH2_STREAM_STDERR);
            stream_set_blocking($stream, true);
            stream_set_blocking($serr, true);
            $cmd = fread($stream, 4096);
            $cmderr = fread($serr, 4096);
            fclose($stream);

            if (!empty(trim($cmd))) {
                $this->logger->logWarn(
                    "\t[local] Failed Initialising Provender"
                );
                continue;
            } else {
                $this->logger->logInfo(
                    "\t[local] Provender installation completed"
                );
                yield $host => $conn;
            }

        }
    }
}
