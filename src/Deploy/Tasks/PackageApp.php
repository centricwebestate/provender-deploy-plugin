<?php

namespace CWE\Provender\Plugins\Deploy\Tasks;

use CWE\Provender\Interfaces\Task;
use CWE\Libraries\EventEmitter\Event;
use CWE\Provender\CLI;
use CWE\Libraries\ObjectRex;
use ZipArchive;
use RecursiveIteratorIterator;
use RecursiveDirectoryIterator;

class PackageApp implements Task
{
    protected $eventEmitter;
    protected $logger;
    protected $config;
    protected $uid;

    public function __construct()
    {
        
    }

    public function setUID($uid)
    {
        $this->uid = $uid;
    }

    public function getUID()
    {
        return $this->uid;
    }

    public function run(array &$resources, array $options, array $results = [])
    {
        $this->logger = $resources['logger'];
        $this->eventEmitter = &$resources['eventEmitter'];
        $this->config = $resources['config']['deploy'];

        $provenderDir = CLI::findProjectFolder(__DIR__);
        $projectFolder = dirname($provenderDir);
        if (!$projectFolder) {
            $this->logger->logError(
                "\t[local] Cant seem to find the project folder"
            );
            $this->logger->log();
            return false;
        }

        $this->logger->logInfo(
            "\t[local] Project folder found, packaging..."
        );

        $releasesFolder = "$provenderDir/releases";
        if (!file_exists($releasesFolder)) {
            $this->logger->logInfo(
                "\t[local] Creating release folder"
            );
            mkdir($releasesFolder);
        }

        date_default_timezone_set('Etc/UTC');
        $zipHash = date('YmdB');
        
        $zipLocation = "$releasesFolder/$zipHash.zip";
        // if (!copy($zipName, $zipLocation)) {
        //     $this->logger->logError(
        //         "\t[local] Unable to place package in releases directory"
        //     );
        //     $this->logger->log();
        //     return false;
        // }
        $relativeZip = substr($zipLocation, strlen(getcwd()));
        $relativeProjectFolder = substr($projectFolder, strlen(getcwd()));
        $output = `zip -9 -r --exclude=*.provender/releases*  ./$relativeZip ./$relativeProjectFolder/`;
        if (!empty($output)) {
            $this->logger->logInfo(
                "\t[local] Package now in releases"
            );
        } else {
            $this->logger->logError(
                "\t[local] Package creation failed"
            );
            return false;
        }


        return $zipLocation;
    }
}
