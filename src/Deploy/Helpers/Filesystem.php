<?php

namespace CWE\Provender\Plugins\Deploy\Helpers;

class Filesystem
{
    public static function expandPath($path)
    {
        if (function_exists('posix_getuid') && strpos($path, '~') !== false) {
            $info = posix_getpwuid(posix_getuid());
            $path = str_replace('~', $info['dir'], $path);
        }

        return $path;
    }
}
