<?php

namespace CWE\Provender\Plugins\Deploy\HelpTexts;

class StartInformation
{
    public function getHelp($event, $logger)
    {
        $logger->log();
        $logger->logInfo('Deploys project to a particular app set');
        $logger->log();
        $logger->logInfo('Usage:');
        $logger->log(
            "  provender deploy:start <app_set>"
        );
        $logger->log();
        $logger->logInfo('Arguments:');
        $logger->log(
            "  app_set \tThe app_set to deploy to as specified in the config"
        );
        $logger->log();
    }
}
