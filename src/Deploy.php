<?php

namespace CWE\Provender\Plugins;

use CWE\Provender;
use CWE\Provender\Command;
use CWE\Provender\Plugins\Deploy\Tasks\CheckStrategy;
use CWE\Provender\Plugins\Deploy\Tasks\CheckAppSet;
use CWE\Provender\Plugins\Deploy\Tasks\CheckKeys;
use CWE\Provender\Plugins\Deploy\Tasks\CleanReleases;
use CWE\Provender\Plugins\Deploy\Tasks\PackageApp;
use CWE\Provender\Plugins\Deploy\Tasks\MakeConnections;
use CWE\Provender\Plugins\Deploy\Tasks\SendApp;
use CWE\Provender\Plugins\Deploy\Tasks\CheckProvender;
use CWE\Provender\Plugins\Deploy\Tasks\LinkRelease;
use CWE\Provender\Plugins\Deploy\Tasks\SendComplete;
use CWE\Provender\Plugins\Deploy\HelpTexts\StartInformation;
use CWE\Libraries\ObjectRex;
use GetOptionKit\OptionCollection;

class Deploy
{
    protected $resources = [];

    public function __construct(Provender $provender)
    {
        $this->addResource('config', $provender->getConfig());
        $this->addResource('eventEmitter', $provender->getEventEmitter());
        $this->addResource('logger', $provender->getLogger());

        $provender->registerCommand(
            new ObjectRex('/^deploy:start$/'),
            $this->getLocalCommand()
        );

        $helpinfo = new StartInformation();
        $provender->getEventEmitter()->addListener(
            'help:deploy:start',
            [$helpinfo, 'getHelp'],
            [$provender->getLogger()]
        );

        $provender->registerCommand(
            new ObjectRex('/^deploy:aquire$/'),
            $this->getRemoteCommand()
        );
    }

    protected function addResource($name, $resource)
    {
        $this->resources[$name] = $resource;
    }

    protected function &getResources()
    {
        return $this->resources;
    }

    protected function getLocalCommand()
    {
        $logger = $this->resources['logger'];
        $this->resources['eventEmitter']->addListener(
            'beforeRun',
            function ($event) use ($logger) {
                $data = $event->getData();
                if (
                    isset($data['self']->name)
                    && $data['self']->name == 'deploy:start'
                ) {
                    $logger->logInfo(
                        '[local] Deployment Starting'
                    );
                }
            }
        );

        $checkStrategy = new CheckStrategy();
        $checkAppSet = new CheckAppSet();
        $checkKeys = new CheckKeys();
        $cleanReleases = new CleanReleases();
        $packageApp = new PackageApp();
        $makeConnections = new MakeConnections();
        $checkProvender = new CheckProvender();
        $sendApp = new SendApp();

        $command = new Command($this->getResources());
        $command->setDescription('Starts the deployment');
        $command->setOptions($this->getDeployOptions());
        $command->addTask($checkAppSet);
        $command->addTask($checkStrategy);
        $command->addTask($checkKeys);
        $command->addTask($cleanReleases);
        $command->addTask($packageApp);
        $command->addTask($makeConnections);
        $command->addTask($checkProvender);
        $command->addTask($sendApp);
        $command->name = 'deploy:start';
        return $command;
    }

    protected function getRemoteCommand()
    {
        $sendComplete = new SendComplete();
        $linkRelease = new LinkRelease();

        $command = new Command($this->getResources());
        $command->setDescription('Finishes the deployment');
        $command->setOptions($this->getAquireOptions());

        $command->addTask($linkRelease);
        $command->addTask($sendComplete);
        return $command;
    }

    protected function getDeployOptions()
    {
        $options = new OptionCollection();
        $options->add('clean', 'Clean the releases folder')->isa('Boolean');
        return $options;
    }

    protected function getAquireOptions()
    {
        $options = new OptionCollection();
        $options->add('release:', 'Specify the release to aquire')->isa('String');
        return $options;
    }
}
